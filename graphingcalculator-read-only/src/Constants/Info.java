/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Constants;

/**
 * Contains info for info tabs.
 * @author Egor
 */
public class Info {

    public static final String ABOUT = "Egor's Graphing Calculator is a desktop calculator \nthat implements the functionality of a TI-83.";
    public static final String HELP = "Supports the following functions: \n\n"
            + "*, +, -, *, /, ^, %, cos, sin, tan, acos, asin, atan,\n" + "sqrt, sqr, log, min, max, ceil, floor, abs, neg, rnd." +
            "\n\n" +
            "When in graph mode, click and drag the mouse to move the graph plane.";
}
